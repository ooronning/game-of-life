﻿namespace GameOfLife.TestLibrary
{
    public class Counter
    {
        private int counter = 0;
        public int Iterate()
        {
            counter += 1;
            return counter;
        }
    }
}
