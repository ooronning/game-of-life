﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameOfLife.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FieldController : ControllerBase
    {
        [HttpPost("file_upload")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> UploadFieldFile([FromForm] IFormFile file)
        {
            var message = "";

            using (var readStream = file.OpenReadStream())
            {
                using (var reader = new StreamReader(readStream))
                {
                    var counter = 0;
                    while (!reader.EndOfStream)
                    {
                        var line = await reader.ReadLineAsync();

                        if (counter == 0 && line != "#Life 1.06")
                        {
                            return BadRequest(new Result
                            {
                                Message = "Only Life 1.06 format is accepted."
                            });
                        }
                        counter++;

                        message += line;
                    }
                }
            }

            return Ok(new Result
            {
                Message = message
            });
        }

        class Result
        {
            public string Message { get; set; }
        }
    }
}