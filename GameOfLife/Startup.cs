﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using GameOfLife.Hubs;
using Microsoft.AspNetCore.Cors.Infrastructure;

namespace GameOfLife
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.WithOrigins(new[] { "http://localhost:63342", "null" });
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowCredentials();
            corsBuilder.AllowAnyMethod();

            services.AddCors(options =>
            {
                options.AddPolicy("BrowserTestPolicy", corsBuilder.Build());
            });
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }


            app.UseHttpsRedirection();
            //app.UseCors(builder => builder.WithOrigins("http://localhost:63342")
            //                              .AllowAnyHeader()
            //                              .AllowAnyMethod());
            app.UseCors("BrowserTestPolicy");
            app.UseSignalR(routes =>
            {
                routes.MapHub<TestHub>("/test-hub");
                routes.MapHub<FieldHub>("/game-of-life/field");
            });
            app.UseMvc();
        }
    }
}
