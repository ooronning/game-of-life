﻿namespace GameOfLife.Enums
{
    public enum CellState
    {
        Alive = 0,
        Dead = 1
    }
}
