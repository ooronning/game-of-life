﻿namespace GameOfLife.Enums
{
    public enum PatternsEnum
    {
        Blinker,
        Block,
        Glider,
        Tub,
        Beacon,
        QueenBee,
        GliderIntoBlock
    }
}
