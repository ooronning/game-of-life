﻿using GameOfLife.Enums;

namespace GameOfLife.Library
{
    public static class PatternGenerator
    {
        public static void GeneratePattern(PatternsEnum pattern, Field field)
        {
            switch (pattern)
            {
                case PatternsEnum.Glider:
                    Glider(field);
                    break;
                case PatternsEnum.Blinker:
                    Blinker(field);
                    break;
                case PatternsEnum.QueenBee:
                    QueenBee(field);
                    break;
                case PatternsEnum.GliderIntoBlock:
                    Glider(field);
                    CornerBlock(field, (field.Width - 10, field.Height - 10));
                    break;
                default:
                    return;
            }
        }

        public static void Glider(Field field)
        {
            var (x, y) = GetMidpoint(field.Width, field.Height);
            field.UpdateCell(x - 1, y + 1);
            field.UpdateCell(x, y - 1);
            field.UpdateCell(x, y + 1);
            field.UpdateCell(x + 1, y);
            field.UpdateCell(x + 1, y + 1);
        }

        public static void Blinker(Field field)
        {
            var (x, y) = GetMidpoint(field.Width, field.Height);
            field.UpdateCell(x - 1, y);
            field.UpdateCell(x, y);
            field.UpdateCell(x + 1, y);
        }

        public static void QueenBee(Field field)
        {
            var (x, y) = GetMidpoint(field.Width, field.Height);
            field.UpdateCell(x - 2, y - 3);
            field.UpdateCell(x - 2, y - 2);
            field.UpdateCell(x - 2, y + 2);
            field.UpdateCell(x - 2, y + 3);
            field.UpdateCell(x - 1, y - 1);
            field.UpdateCell(x - 1, y);
            field.UpdateCell(x - 1, y + 1);
            field.UpdateCell(x, y - 2);
            field.UpdateCell(x, y + 2);
            field.UpdateCell(x + 1, y - 1);
            field.UpdateCell(x + 1, y + 1);
            field.UpdateCell(x + 2, y);
        }

        //public static void GliderIntoBlock(Field field)
        //{
        //    Glider(field);
        //    field.UpdateCell(field.Width, field.Height);
        //    field.UpdateCell(field.Width, field.Height - 1);
        //    field.UpdateCell(field.Width - 1, field.Height);
        //    field.UpdateCell(field.Width - 1, field.Height - 1);
        //}
        
        public static void CornerBlock(Field field, (int x, int y) topLeft)
        {
            var (x, y) = topLeft;
            
            field.UpdateCell(x - 1, y - 1);
            field.UpdateCell(x - 1, y - 2);
            field.UpdateCell(x - 2, y - 1);
            field.UpdateCell(x - 2, y - 2);
        }

        private static (int x, int y) GetMidpoint(int width, int height)
        {
            return (width / 2, height / 2);
        }
    }
}
