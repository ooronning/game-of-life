﻿using System;

namespace GameOfLife.Library
{
    public class Cell : IEquatable<Cell>
    {
        public bool Alive { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public Cell(bool alive, int x, int y)
        {
            Alive = alive;
            X = x;
            Y = y;
        }

        public bool Equals(Cell cell) => cell.X == X && cell.Y == Y && cell.Alive == Alive;
        public bool IsInPosition(Cell cell) => cell.X == X && cell.Y == Y;
    }
}
