﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameOfLife.Enums;

namespace GameOfLife.Library
{
    public class Field
    {
        public List<List<Cell>> Cells { get; }
        public int Width { get; }
        public int Height { get; }

        private delegate Cell MockCell(int x, int y, bool alive = true);

        private readonly Queue<EnqueuedUpdate> _updatesQueue = new Queue<EnqueuedUpdate>();
        private readonly static List<MockCell> _mockFunctions = new List<MockCell>
        {
            MockLeftCell,
            MockLeftUpCell,
            MockUpCell,
            MockRightUpCell,
            MockRightCell,
            MockRightDownCell,
            MockDownCell,
            MockDownLeftCell
        };

        public Field(int width, int height)
        {
            var rows = new List<List<Cell>>(height);
            foreach (var i in Enumerable.Range(0, height))
            {
                var column = new List<Cell>();
                foreach (var j in Enumerable.Range(0, width))
                {
                    column.Add(new Cell(false, i, j));
                }

                rows.Add(column);
            }

            Cells = rows;
            Width = width;
            Height = height;
        }

        public void Reset()
        {
            foreach (var row in Cells)
            {
                foreach (var cell in row)
                {
                    cell.Alive = false;
                }
            }
        }

        public void UpdateCell(int row, int column, bool isAlive = true)
        {
            Cells[row][column].Alive = isAlive;
        }

        public Cell GetCell(int row, int column)
        {
            return Cells[row][column];
        }

        public void ScanAndUpdateField()
        {
            // scan for necessary updates and enqueue
            var flattenedCells = Cells.SelectMany(x => x).ToList<Cell>();
            var liveCells = GetLiveAndAdjacentCells();
            foreach (var cell in liveCells)
            {
                var liveNeighbors = GetLiveNeighborsCount(cell, liveCells);
                if (cell.Alive && (liveNeighbors < 2 || liveNeighbors > 3)) _updatesQueue.Enqueue(new EnqueuedUpdate(cell, false));
                if (!cell.Alive && liveNeighbors == 3) _updatesQueue.Enqueue(new EnqueuedUpdate(cell, true));
            }

            // run each enqueued update
            while (_updatesQueue.Count > 0)
            {
                var update = _updatesQueue.Dequeue();
                var cell = flattenedCells.Find(x => x.IsInPosition(update.Cell));
                cell.Alive = update.AliveNextState;
            }
        }

        private List<Cell> GetLiveAndAdjacentCells()
        {
            var list = Cells.SelectMany(x => x).ToList<Cell>().FindAll(x => x.Alive);
            var deadList = new List<Cell>();
            foreach (var cell in list)
            {
                foreach (var f in _mockFunctions)
                {
                    var neighbor = f(cell.X, cell.Y, false);
                    if (!list.Contains(neighbor))
                    {
                        deadList.Add(neighbor);
                    }
                }
            }

            return list.Union(deadList).ToList();
        }

        private int GetLiveNeighborsCount(Cell cell, List<Cell> list)
        {
            var live = 0;

            foreach (var f in _mockFunctions)
            {
                var neighbor = f(cell.X, cell.Y);
                if (neighbor.X < 0 || neighbor.Y < 0 || neighbor.X >= Width || neighbor.Y >= Height) continue;
                if (list.Contains(neighbor))
                {
                    live++;
                }
            }

            return live;
        }

        private static Cell MockLeftCell(int x, int y, bool alive = true) => new Cell(alive, x - 1, y);
        private static Cell MockLeftUpCell(int x, int y, bool alive = true) => new Cell(alive, x - 1, y - 1);
        private static Cell MockUpCell(int x, int y, bool alive = true) => new Cell(alive, x, y - 1);
        private static Cell MockRightUpCell(int x, int y, bool alive = true) => new Cell(alive, x + 1, y - 1);
        private static Cell MockRightCell(int x, int y, bool alive = true) => new Cell(alive, x + 1, y);
        private static Cell MockRightDownCell(int x, int y, bool alive = true) => new Cell(alive, x + 1, y + 1);
        private static Cell MockDownCell(int x, int y, bool alive = true) => new Cell(alive, x, y + 1);
        private static Cell MockDownLeftCell(int x, int y, bool alive = true) => new Cell(alive, x - 1, y + 1);
    }

    class EnqueuedUpdate
    {
        public Cell Cell { get; set; }
        public bool AliveNextState { get; set; }

        public EnqueuedUpdate(Cell cell, bool alive)
        {
            Cell = cell;
            AliveNextState = alive;
        }
    }
}
