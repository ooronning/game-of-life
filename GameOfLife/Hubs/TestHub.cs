﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using GameOfLife.TestLibrary;

namespace GameOfLife.Hubs
{
    public class TestHub : Hub
    {
        private static Counter _counter = new Counter();

        public async Task SendCount()
        {
            await Clients.All.SendAsync("ReceiveCount", _counter.Iterate());
        }
    }
}
