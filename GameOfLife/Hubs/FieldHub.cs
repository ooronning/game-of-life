﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Newtonsoft.Json;
using GameOfLife.Library;
using GameOfLife.Enums;

namespace GameOfLife.Hubs
{
    public class FieldHub : Hub 
    {
        // refactor to enable infinite grid
        private static Field _field = new Field(50, 50);

        public async Task InitializeField(PatternsEnum pattern)
        {
            _field.Reset();

            PatternGenerator.GeneratePattern(pattern, _field);
            var response = JsonConvert.SerializeObject(_field);
            await Clients.Caller.SendAsync("ReceiveField", response);
        }

        public async Task UpdateField()
        {
            _field.ScanAndUpdateField();
            var response = JsonConvert.SerializeObject(_field);
            await Clients.All.SendAsync("ReceiveField", response);
        }
    }
}
